function changeProcessText(){
	//This script modifies the process text
	var procText = document.getElementById("processText");//getting text
	var bar = document.getElementById("progressBar");//getting bar
	
	//modify text
	switch (bar.value) {
		case 0:
			procText.innerHTML = "Detecting your wish";
			break;
		case 20:
			procText.innerHTML = "Consult with miracle workers";
			break;
		case 40:
			procText.innerHTML = "Summoning good spirits";
			break;
		case 60:
			procText.innerHTML = "Drive away the negative";
			break;
		case 80:
			procText.innerHTML = "Make a wish come true";
			break;
	}
}

function runningDots(){
	//This script make dots on progress bar run
	var dot1 = document.getElementById("processTextDot1");//getting dots
	var dot2 = document.getElementById("processTextDot2");
	var dot3 = document.getElementById("processTextDot3");
	var bar = document.getElementById("progressBar");//getting progress bar

	var currentDot = 1;//counter
	function timerDotsRun(){
		if (bar.value >= 100){//checking, if progress is over, stop dots from running
				clearInterval(dotTimer);
				dot1.style.visibility='visible';
				dot2.style.visibility='visible';
				dot3.style.visibility='visible';
				currentDot = 1;
		}
		else {//if progress is not over
				switch(currentDot){
			case 1://hide all
				dot1.style.visibility='hidden';
				dot2.style.visibility='hidden';
				dot3.style.visibility='hidden';
				currentDot++;//increase counter
				break;
			case 2://show first dot
				dot1.style.visibility='visible';
				dot2.style.visibility='hidden';
				dot3.style.visibility='hidden';
				currentDot++;
				break;
			case 3://show second dot
				dot1.style.visibility='visible';
				dot2.style.visibility='visible';
				dot3.style.visibility='hidden';
				currentDot++;
				break;
			case 4://show all dots and set counter to beginning
				dot1.style.visibility='visible';
				dot2.style.visibility='visible';
				dot3.style.visibility='visible';
				currentDot = 1;
				break;
			}
		}
	}

	var dotTimer = setInterval(timerDotsRun, 500);//timer, call dots running function every 0.5 sec
}

function activProgressBar(){
	//This script activates progress bar and show progress window
	var procDiv = document.getElementsByClassName("processDiv");//getting window
	var bar = document.getElementById("progressBar");//getting bar
	var mainButtin = document.getElementById("mainButton");//getting main button
	var upperBar = document.getElementsByClassName("upperBar");//getting upper bar
	var upperBarCell = document.getElementsByClassName("upperBarTableCells");//get upper bar cells

	function moveBar (){
		//call change text function
		changeProcessText();
			//end of process, stop bar and hide window
			if (bar.value >= 100){
				clearInterval(timer);
				alert("Your wish has come true!");
				bar.value = 0;
				mainButtin.style.pointerEvents = 'auto';//enable main button after finishing
				procDiv[0].style.display='none';
				upperBar[0].classList.add("upperBarHover");//make upper bar movable again
				upperBarCell[3].style.opacity = '1';//show upper bar last cell
				upperBarCell[3].style.pointerEvents = 'auto';//enable pointe events upper bar last cell
			}
			//if bar is not full, show window and run bar
			else {
				mainButtin.style.pointerEvents = 'none';//disable main button, while process is going
				procDiv[0].style.display='block';//show process div
				bar.value += 1;
				upperBar[0].classList.remove("upperBarHover");//make upper bar unmovable during process
				upperBarCell[3].style.opacity = '0';//hide upper bar last cell
				upperBarCell[3].style.pointerEvents = 'none';//disable pointe events upper bar last cell
			}
		}

	var timer = setInterval(moveBar, 100);//timer, call progress bar running function every 0.1 sec
}

function showCells() {
	//show cells in Upper Div
	var cells = document.getElementsByClassName("cellDisappear");//getting cells
	for (var i = 0; i < cells.length; i++) {
		cells[i].style.opacity='1';//make cells visible
	}
}

function hideCells() {
	//hide cells in Upper Div and make own text input empty
	var cells = document.getElementsByClassName("cellDisappear");//getting cells
	var input = document.getElementById("ownTextInput");//getting input
	for (var i = 0; i < cells.length; i++) {
		cells[i].style.opacity='0';//hide cells 
	}
	input.value = "";//clear own text value
}

function setNewText(x) {
	//change text in main button
	var cells = document.getElementsByClassName("upperBarTableCells");//getting cells
	var button = document.getElementById("mainButton");//getting main button

	button.innerHTML = cells[x].innerHTML;//adding text
}

function setOwnInputText() {
	//change text in main button to custom
	var input = document.getElementById("ownTextInput");//getting input
	var button = document.getElementById("mainButton");//getting main button
	if (input.value != "") {
		button.innerHTML = input.value; //if input not empty, set value to button
	}
	input.value = "";//clear own text value
}